﻿using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using Raven.Client;
using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using NsbDemo.Contracts.Messages;

namespace NsbDemo.UI.Controllers
{
    public class ApplicationController : Controller
    {
        readonly IDocumentSession session;

        public ApplicationController(IDocumentSession session)
        {
            this.session = session;
        }

        #region Get
        public ActionResult Index()
        {
            return View(Application.Create("Mr", "Kelly", "Cliffe", "RC454", "Here", "There", "8080", "Chch", "...@gmail.com"));
        }

        public ActionResult ImagesPending()
        {
            var appsPending = session.Query<Application>().Where(a => a.Status == ApplicationStatus.PendingImageVerification);

            return View(appsPending);
        }

        [HttpGet]
        public ActionResult ApplicationsExpired()
        {
            var appsExpired = session.Query<Application>().Where(a => a.Status == ApplicationStatus.Expired);

            return View(appsExpired);
        }
        #endregion

        #region Post
        [HttpPost]
        public JsonResult Create(string title,
            string firstName,
            string lastName,
            string refereeCardReference,
            string addressLine1,
            string addressLine2,
            string postCode,
            string city,
            string emailAddress,
            DateTime? dateOfBirth)
        {
            // Send() does not require explicit knowledge of the destination queue. This is defined via routing configuration.
            Helpers.GetBus()
                .Send(new ReceiveApplicationCommand(title, firstName, lastName, refereeCardReference, addressLine1, addressLine2,
                    postCode, city, emailAddress));

            return Json(new { Msg = "Ok" });
        }

        // Yep - it should be post since we'll be altering state. Demo disclaimer...
        [HttpGet]
        public void ImageProcessed(string applicationId)
        {
            Helpers.GetBus()
                .Send(new ApplicationImageProcessedCommand(applicationId));
        }

        [HttpGet]
        public void Test()
        {
            Helpers.GetBus().Send(new TestMessage());
        }

        #endregion
    }
}
