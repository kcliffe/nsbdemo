﻿using NServiceBus;
using Raven.Client;
using System.Web.Mvc;

namespace NsbDemo.UI.Controllers
{
    public class HomeController : Controller
    {
        readonly IDocumentSession session;

        public HomeController(IDocumentSession session)
        {
            this.session = session;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome the the new improved NsbDemo portal!";

            ViewBag.MemberQualifiesForGoldCard = false;
            if (User.Identity.IsAuthenticated)
            {
                var member = new UIMember(session).Get(User.Identity.Name);

                if (member != null)
                {
                    ViewBag.MemberQualifiesForGoldCard = member.QualifiesAsGoldCardMember;
                    ViewBag.MembershipNumber = member.MembershipNumber;
                }
            }

            return View();
        }
    }

    public static class Helpers
    {
        public static IBus GetBus()
        {
            return Configure
             .With()
             .DefaultBuilder()
             .MsmqTransport()
                .IsTransactional(true)
             .UnicastBus()
                .ImpersonateSender(false)
             .JsonSerializer()
             .SendOnly();
        }
    }
}
