﻿using NsbDemo.Core.Entities;
using Raven.Client;
using System.Linq;

namespace NsbDemo.UI
{
    public class UIMember
    {
        readonly IDocumentSession session;

        public UIMember(IDocumentSession session)
        {
            this.session = session;
        }

        public Member Get(string membershipIdentity)
        {
            return session.Query<Member>().FirstOrDefault(a => a.EmailAddress == membershipIdentity);
        } 
    }
}