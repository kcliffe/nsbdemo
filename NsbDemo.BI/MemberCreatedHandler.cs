﻿using NsbDemo.Contracts.Integration.Events;
using NsbDemo.Core;
using NServiceBus;
using Raven.Client;

namespace NsbDemo.BI
{
    public class NsbDemoCardCreatedHandler : IHandleMessages<NsbDemoCardCreatedEvent>
    {
        private readonly IDocumentSession session;

        public NsbDemoCardCreatedHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(NsbDemoCardCreatedEvent @event)
        {
            session.Store(new { CardId = @event.CardId, EntityName = "BI Input" });

            Helpers.Out("BI Info received NsbDemoCardCreated event");
        }
    }
}
