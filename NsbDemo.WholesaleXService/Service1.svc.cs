﻿using System;

namespace NsbDemo.WholesaleXService
{
    public class Service1 : ICreditService
    {
        public CreditCheckResponse CheckMemberCredit(CreditCheckRequest request)
        {
            return new CreditCheckResponse
            {
                CreditOk = true
            };
        }
    }
}
