﻿using System.Runtime.Serialization;

namespace NsbDemo.WholesaleXService
{
    [DataContract]
    public class CreditCheckResponse
    {
        [DataMember]
        public bool CreditOk { get; set; }
    }
}
