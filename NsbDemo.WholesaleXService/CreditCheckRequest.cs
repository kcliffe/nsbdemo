﻿using System.Runtime.Serialization;

namespace NsbDemo.WholesaleXService
{
    [DataContract]
    public class CreditCheckRequest
    {
        [DataMember(IsRequired=true)]
        public string MemberId { get; set; }
    }
}
