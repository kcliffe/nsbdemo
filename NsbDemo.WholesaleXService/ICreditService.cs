﻿using System.ServiceModel;

namespace NsbDemo.WholesaleXService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace="http://creditservice.v1")]
    public interface ICreditService
    {
        [OperationContract]
        CreditCheckResponse CheckMemberCredit(CreditCheckRequest request);
    }
}
