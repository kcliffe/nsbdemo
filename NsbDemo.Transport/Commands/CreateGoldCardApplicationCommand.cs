﻿namespace NsbDemo.Contracts.Commands
{
    public class CreateGoldCardApplicationCommand
    {
        public string MembershipNumber { get; set; }

        public CreateGoldCardApplicationCommand(string memberId)
        {
            MembershipNumber = memberId;
        }
    }
}
