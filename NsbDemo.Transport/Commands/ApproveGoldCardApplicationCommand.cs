﻿namespace NsbDemo.Contracts.Commands
{
    public class ApproveGoldCardApplicationCommand
    {
        public string MembershipNumber { get; set; }

        public ApproveGoldCardApplicationCommand(string memberId)
        {
            MembershipNumber = memberId;
        }
    }
}
