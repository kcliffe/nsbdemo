﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NsbDemo.Contracts.Commands
{
    public class TimeoutApplicationCommand
    {
        public string ApplicationReference { get; set; }

        public TimeoutApplicationCommand(string applicationReference)
        {
            ApplicationReference = applicationReference;
        }
    }
}
