﻿using System;

namespace NsbDemo.Contracts.Commands
{
    /// <summary>
    /// An application which has been entered by an applicant. Would typically be immutable.
    /// </summary>
    public class ReceiveApplicationCommand
    {
        public Guid ApplicationId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RefereeCardReference { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string EmailAddress { get; set; }

        public ReceiveApplicationCommand(string title, 
            string firstName, 
            string lastName, 
            string refereeCardReference, 
            string addressLine1, 
            string addressLine2, 
            string postCode, 
            string city,
            string emailAddress)
        {
            Title = title;
            FirstName = firstName;
            LastName = lastName;
            RefereeCardReference = refereeCardReference;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            PostCode = postCode;
            City = city;
            EmailAddress = emailAddress;
        }
    }    
}
