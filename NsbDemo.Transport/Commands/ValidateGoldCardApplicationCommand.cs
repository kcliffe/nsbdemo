﻿namespace NsbDemo.Contracts.Commands
{
    public class ValidateGoldCardApplicationCommand
    {
        public string MembershipNumber { get; set; }

        public ValidateGoldCardApplicationCommand(string membershipNumber)
        {
            MembershipNumber = membershipNumber;
        }
    }
}
