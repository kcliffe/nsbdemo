﻿namespace NsbDemo.Contracts.Commands
{
    public class ReceiveGoldCardApplicationCommand
    {
        public string MembershipNumber { get; set; }
    }
}
