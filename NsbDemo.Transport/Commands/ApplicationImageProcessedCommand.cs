﻿namespace NsbDemo.Contracts.Commands
{
    public class ApplicationImageProcessedCommand
    {
        public string ApplicationReference { get; set; }
        public bool SendResponse { get; set; }

        public ApplicationImageProcessedCommand(string applicationReference)
        {
            ApplicationReference = applicationReference;
        }
    }
}
