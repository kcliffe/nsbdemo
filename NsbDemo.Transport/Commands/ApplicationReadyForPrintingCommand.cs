namespace NsbDemo.Contracts.Commands
{
    public class ApplicationReadyForPrintingCommand
    {
        public string ApplicationReference { get; set; }

        public ApplicationReadyForPrintingCommand(string applicationReference)
        {
            ApplicationReference = applicationReference;
        }
    }
}