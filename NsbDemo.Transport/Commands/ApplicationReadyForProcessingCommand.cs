﻿namespace NsbDemo.Contracts.Commands
{
    //[TimeToBeReceived("00:10:00")]
    public class ProcessApplicationCommand
    {
        public string ApplicationReference { get; set; }

        public ProcessApplicationCommand(string applicationReference)
        {
            ApplicationReference = applicationReference;
        }
    }
}