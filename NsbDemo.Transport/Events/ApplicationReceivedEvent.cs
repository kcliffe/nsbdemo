﻿namespace NsbDemo.Contracts.Events
{
    public class ApplicationReceivedEvent
    {
        public string ApplicationReference { get; set; }

        public ApplicationReceivedEvent(string applicationReference)
        {
            ApplicationReference = applicationReference;
        }
    }
}
