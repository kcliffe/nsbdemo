﻿namespace NsbDemo.Contracts.Events
{
    public class GoldCardRejectedEvent
    {
        public string MemberId { get; set; }

        public GoldCardRejectedEvent(string memberId)
        {
            MemberId = memberId;
        }
    }
}