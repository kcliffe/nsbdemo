﻿namespace NsbDemo.Contracts.Events
{
    public class GoldCardValidationRequestExpiredEvent
    {
        public string MemberId { get; set; }

        public GoldCardValidationRequestExpiredEvent(string memberId)
        {
            MemberId = memberId;
        }
    }
}