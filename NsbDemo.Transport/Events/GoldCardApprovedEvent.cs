﻿namespace NsbDemo.Contracts.Events
{
    public class GoldCardApprovedEvent
    {
        public string MemberId { get; set; }

        public GoldCardApprovedEvent(string memberId)
        {
            MemberId = memberId;
        }
    }
}