﻿namespace NsbDemo.Contracts.Events
{
    public class GoldCardApplicationFailedEvent
    {
        public string MembershipNumber { get; set; }

        public GoldCardApplicationFailedEvent(string membershipNumber)
        {
            MembershipNumber = membershipNumber;
        }
    }
}
