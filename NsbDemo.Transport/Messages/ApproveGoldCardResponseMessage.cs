﻿namespace NsbDemo.Contracts.Messages
{
    public class ApproveGoldCardResponseMessage
    {
        public string MemberId { get; set; }
        public bool Approved { get; set; }
    }
}