﻿using System.Runtime.Remoting.Messaging;

namespace NsdDemo.Transport.Messages
{
    public class GoldCardCreditCheckedResponseMessage
    {
        public string MemberId { get; set; }
        public bool CreditOk { get; set; }
    }
}
