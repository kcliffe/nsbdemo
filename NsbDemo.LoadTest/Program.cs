﻿using NServiceBus;
using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Transport.Commands;
using System;
using System.Threading.Tasks;
using StructureMap;

namespace NsbDemo.LoadTest
{
    class Program
    {
        static void Main(string[] args)
        {            
            const int numMessages = 10;

            var opts = new ParallelOptions { MaxDegreeOfParallelism = 3 };
            Parallel.For(0, numMessages, opts, i =>
            {
                var bus = GetBus();
                var receiveAppCmd = GetApp();

                bus.Send("nsbdemo.processing", receiveAppCmd);
            });
        }

        private static IBus GetBus()
        {
            var xc = Configure.With()
                            .StructureMapBuilder(new Container());
                            //.MsmqTransport().IsTransactional(true)
                            //.UnicastBus()
                            //.JsonSerializer()
                            //.SendOnly();   

            xc.SendOnly();
            return xc.SendOnly();
        }

        private static ReceiveApplicationCommand GetApp()
        {
            return new ReceiveApplicationCommand("Mr", "Frederick", "WeenSprocket", "", "Here", "There",
                                                 "8085", "Christchurch", Application.DemoUserName + "@gmail.com", DateTime.Now, "user1");
        }
    }
}
