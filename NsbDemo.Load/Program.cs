﻿using NsbDemo.Contracts.Commands;
using NServiceBus;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace NsbDemo.Load
{
    class Program
    {
        static void Main(string[] args)
        {
            // 20850 Single worker single thread
            // 15000 Single worker 5 threads
            // X 5 workers 5 threads

            const int max = 100;
            var opts = new ParallelOptions { MaxDegreeOfParallelism = 5 };
            var bus = Helpers.GetBus();

            Parallel.For(0, max, opts, i => 
            {
                bus.Send("nsbdemo.processing", Get());
                Console.WriteLine("Sent");
            });

            Console.WriteLine("All Sent - enter to start timing");

            Console.ReadLine();
            var sw = new Stopwatch();
            sw.Start();

            Console.ReadLine();
            Console.WriteLine(sw.ElapsedMilliseconds);
            sw.Stop();

            Console.ReadLine();
        }
        
        public static ReceiveApplicationCommand Get()
        {
            return new ReceiveApplicationCommand("Mr", "Bob", "Bobson", "x", "Here", "There",
                            "8088", "Chch", "bob.mcbobson@gmail.com");
        }
    }

    public static class Helpers
    {
        public static IBus GetBus()
        {
            return Configure.With()
            .StructureMapBuilder()
            .JsonSerializer()
            .MsmqTransport()
            .UnicastBus()
            .SendOnly();
        }
    }
}
