﻿using System;

namespace NsbDemo.Core
{
    public static class Helpers
    {
        public static void Out(string str, params object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(str, args);
            Console.ResetColor();
        }
    }
}