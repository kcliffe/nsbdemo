namespace NsbDemo.Core 
{
    using NServiceBus;
    using NServiceBus.UnitOfWork;
    using Raven.Client;
    using Raven.Client.Document;
    using StructureMap;
    using System.Reflection;

	/*
		This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
		can be found here: http://nservicebus.com/GenericHost.aspx
	*/
	public class EndpointConfig : IConfigureThisEndpoint, AsA_Server
    {
        /// <summary>
        /// Configure NServiceBus
        /// </summary>
        public void Init()
        {
            Configure
                // Only check this assembly for handlers
                .With(Assembly.GetExecutingAssembly(), Assembly.GetAssembly(typeof(EndpointConfig)))
                // IOC with Structuremap
                .StructureMapBuilder()
                // Use Jason for message serialization
                //.JsonSerializer()
                .MsmqTransport()
                .InMemorySubscriptionStorage()
                .UseInMemoryTimeoutPersister()
                .JsonSerializer()
                // Message defined in this namespace, unobtrusive (don't need to implement IMessage, ICommand..)
                .DefiningMessagesAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Messages"))
                .DefiningCommandsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Commands"))
                .DefiningEventsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Events"))
                .UnicastBus();
        }
    }

    public class SetupContainer : IWantCustomInitialization
    {
        public IBus Bus { get; set; }

        public void Init()
        {
            var store = new DocumentStore { Url = "http://localhost:8085" };
            store.Initialize();

            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c =>
            {
                c.For<IDocumentStore>()
                    .Singleton()
                    .Use(store);

                c.For<IDocumentSession>()
                    .Use(ctx => ctx.GetInstance<IDocumentStore>().OpenSession("BikePartsNZ"));

                c.For<IManageUnitsOfWork>()
                    .Use<RavenUnitOfWork>();
            });

        }
    }
}