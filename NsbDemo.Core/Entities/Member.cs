﻿using System.Collections.Generic;

namespace NsbDemo.Core.Entities
{
    public class Member
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string MembershipNumber { get; private set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Address Address { get; set; }

        public string EmailAddress { get; set; }

        public List<string> Recommendations { get; set; }

        public string GoldCardId { get; set; }

        public string Name
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        public bool QualifiesAsGoldCardMember
        {
            get
            {
                return string.IsNullOrEmpty(GoldCardId) && Recommendations.Count >= 5;
            }
        }

        public Member()
        {
            Recommendations = new List<string>();
        }

        public Member(Application application) : this()
        {
            Address = application.Applicant.Address;
            UserName = application.Applicant.UserName;

            // todo this should be assigned here            
            MembershipNumber = application.Applicant.MembershipNumber;
            Title = application.Applicant.Title;
            FirstName = application.Applicant.FirstName;
            LastName = application.Applicant.LastName;
            EmailAddress = application.Applicant.EmailAddress;
        }
    }
}
