﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NsbDemo.Core.Entities
{
    // Order is important. Handlers may use the enum to verify an operation is IdImpotent
    public enum ApplicationStatus
    {
        Created,        
        PendingImageVerification,
        Expired,
        Processing,
        PendingProcessing,
        ReadyForPrinting,
        PendingShipping,
        Shipped
    }

    public class Application
    {
        /// <summary>
        /// The unique application identifier
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// The applicant details
        /// </summary>
        public Applicant Applicant { get; private set; }

        // Require a signed in customer - no guests.
        public string RefereeCardReference { get; private set; }

        /// <summary>
        /// Date the application was received (at this handler)
        /// </summary>
        public DateTime DateReceived { get; private set; }

        /// <summary>
        /// The current status
        /// </summary>
        public ApplicationStatus Status { get { return StatusHistory.Last(); } }

        /// <summary>
        /// The historical status
        /// </summary>
        public List<ApplicationStatus> StatusHistory { get; private set; }

        /// <summary>
        /// True if the application is still awaiting a processed image
        /// </summary>
        public bool IsStillPendingImage
        {
            get
            { 
                return Status == ApplicationStatus.PendingImageVerification; 
            }
        }

        /// <summary>
        /// True if the image has been printed
        /// </summary>
        public bool HasBeenPrinted
        {
            get
            {
                return Status > ApplicationStatus.ReadyForPrinting;
            }
        }

        public Application()
        {
            StatusHistory = new List<ApplicationStatus>();
        }

        public Application(Applicant applicant, string refereeCardReference)
        {
            Applicant = applicant;
            RefereeCardReference = refereeCardReference;
            StatusHistory = new List<ApplicationStatus>();
            StatusHistory.Add(ApplicationStatus.Created);
            DateReceived = DateTime.Now;
        }

        /// <summary>
        /// Create a new instance of an application
        /// </summary>        
        public static Application Create(string title,
            string firstName,
            string lastName,
            string refereeCardReference,
            string addressLine1,
            string addressLine2,
            string postCode,
            string city,
            string emailAddress)
        {
            var applicant = new Applicant
            {
                Title = title,
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = emailAddress,
                Address = new Address
                {
                    AddressLine1 = addressLine1,
                    AddressLine2 = addressLine2,
                    PostCode = postCode,
                    City = city
                }
            };

            var application = new Application(applicant, refereeCardReference);
            return application;
        }

        /// <summary>
        /// Set the application status to Received
        /// </summary>
        public void Recieve()
        {
            StatusHistory.Add(ApplicationStatus.PendingImageVerification);
        }

        /// <summary>
        /// Set the application status to Expired
        /// </summary>
        public void Expire()
        {
            StatusHistory.Add(ApplicationStatus.Expired);
        }

        /// <summary>
        /// Set the application status to ImageProcessed
        /// </summary>
        public void ImageProcessed()
        {
            StatusHistory.Add(ApplicationStatus.PendingProcessing);
        }

        /// <summary>
        /// Process the application
        /// </summary>
        public bool Process()
        {
            StatusHistory.Add(ApplicationStatus.Processing);

            // Do processy stuff
            // ...

            // On success :-)
            StatusHistory.Add(ApplicationStatus.ReadyForPrinting);

            return true;
        }
    }
}
