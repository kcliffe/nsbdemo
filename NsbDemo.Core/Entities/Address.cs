﻿namespace NsbDemo.Core.Entities
{
    public class Address
    {
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string PostCode { get; set; }

        public string City { get; set; }
    }
}
