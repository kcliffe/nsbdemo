﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NsbDemo.Core.Entities
{
    enum GoldCardStatus
    {
        Rejected,
        Accepted
    }

    class GoldCardRequest
    {
        public string Id { get; set; }
        public GoldCardStatus Status { get; set; }
    }
}
