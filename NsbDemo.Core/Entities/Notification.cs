﻿
namespace NsbDemo.Core.Entities
{
    public class SystemNotification
    {
        public string To { get; set; }

        public string Content { get; set; }

        public string Subject { get; set; }
    }
}
