﻿using System;
using System.Collections.Generic;

namespace NsbDemo.Core.Entities
{
    public class Applicant
    {       
        public string Id { get; set; }

        public string UserName { get; set; }

        public string MembershipNumber { get; private set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Address Address { get; set; }

        public string EmailAddress { get; set; }

        public DateTime DateOfBirth { get; set; }
        
        public string Name
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
    }
}
