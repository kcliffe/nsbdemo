﻿using log4net;
using NServiceBus;
using System.Reflection;

namespace NsbDemo.Core.Auth
{
    public class AuthHandler : IHandleMessages<IMessage>
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IAuthService authService;
        public IBus Bus { get; set; }
        public const string AuthHeaderKey = "auth";

        public AuthHandler(IAuthService authService)
	    {
            this.authService = authService;  
	    }

        public void Handle(IMessage message)
        {
            var authHeader = message.GetHeader(AuthHeaderKey);

            if (!authService.Authorise(authHeader))
            {
                Log.Info("Not Authorised");
                Bus.DoNotContinueDispatchingCurrentMessageToHandlers();
                return;
            }

            Log.Info("Authorised");
        }
    }
}
