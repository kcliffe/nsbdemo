﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NsbDemo.Core.Auth
{
    public interface IAuthService
    {
        bool Authorise(string authValue);
    }
}
