﻿namespace NsbDemo.Contracts.Integration.Events
{
    public class NsbDemoCardCreatedEvent
    {
        public string MemberId { get; set; }
        public string CardId { get; set; }

        public NsbDemoCardCreatedEvent(string memberId, string cardId)
        {
            CardId = cardId;
            MemberId = memberId;
        }

    }
}
