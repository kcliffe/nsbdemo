﻿using System;
using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Events;
using NServiceBus;
using Raven.Client;

namespace NsbDemo.Notifications
{
    public class NotifyApplicantApplicationReceivedHandler : IHandleMessages<ApplicationReceivedEvent>
    {
        private readonly IDocumentSession session;
        private readonly INotificationComponent notifier;
        public IBus Bus { get; set; }

        public NotifyApplicantApplicationReceivedHandler(IDocumentSession session, INotificationComponent notifier)
        {
            this.session = session;
            this.notifier = notifier;
        }

        public void Handle(ApplicationReceivedEvent @event)
        {
            var application = session.Load<Application>(@event.ApplicationReference);
           
            // Create a templated notification message
            const string notification = "<p>Thanks @Model.Name,</p><p>We received your application for an NsbDemo card.</p>";
            // new TemplateContent(session).Fetch("systemNotifications/ApplicationReceived");
           
            // Fully resolve context for templated email
            var context = new {
                Name = application.Applicant.Name,
                DateReceived = application.DateReceived
            };

            var template = new RazorEngine.Templating.TemplateService();
            var resolvedContent = template.Parse(notification, context, null, null);

            // Hardcoded delivery for demo
            notifier.Deliver(resolvedContent, application.Applicant.EmailAddress, "Your application has been received.");

            Helpers.Out("Notified applicant {0} application received", @event.ApplicationReference);
        }
    }
}
