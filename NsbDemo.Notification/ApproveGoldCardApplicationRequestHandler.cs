﻿using System.Linq;
using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NServiceBus;
using Raven.Client;

namespace NsbDemo.Notifications
{
    public class ApproveGoldCardApplicationRequestHandler : IHandleMessages<ApproveGoldCardApplicationCommand>
    {
        private readonly IDocumentSession session;
        private readonly INotificationComponent notifier;

        public ApproveGoldCardApplicationRequestHandler(IDocumentSession session, INotificationComponent notifier)
        {
            this.notifier = notifier;
            this.session = session;
        }

        public void Handle(ApproveGoldCardApplicationCommand command)
        {            
            var member = session.Query<Member>().First(m => m.MembershipNumber == command.MembershipNumber);

            // Create a templated notification message
            const string messageContent = "<p>@Model.Title @Model.Name has requested a gold card.</p><ul>@foreach {var r in @Model.Recommendations} {  <li>@r</li> }</ul>";
            //new TemplateContent(session).Fetch("systemNotifications/ApproveGoldCardRequest");
           
            // Fully resolve context for templated email
            var context = new {
                Title = member.Title,
                Name = member.Name,
                Recommendations = member.Recommendations
            };

            var template = new RazorEngine.Templating.TemplateService();
            var resolvedContent = template.Parse(messageContent, context, null, null);

            // Hardcoded delivery for demo
            notifier.Deliver(resolvedContent, "Nsbdemo.", "A gold card application requires approval.");

            Helpers.Out("Gold card approval notification sent for member {0}", command.MembershipNumber);
        }
    }
}
