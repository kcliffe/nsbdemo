﻿ using Raven.Client;
using System;

namespace NsbDemo.Notifications
{
    public class TemplateContent
    {
        private readonly IDocumentSession session;

        public string Content { get; set; }

        public TemplateContent(IDocumentSession session)
        {
            this.session = session;
        }

        public string Fetch(string notificationKey)
        {
            var template = session.Load<TemplateContent>(notificationKey);
            if (template == null)
                throw new Exception(string.Format(Resource1.error_invalidTemplate, notificationKey));

            return template.Content;
        }
    }
}
