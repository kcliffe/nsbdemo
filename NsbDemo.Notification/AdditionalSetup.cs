﻿using NsbDemo.Notifications.Notifiers;
using NServiceBus;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NsbDemo.Notifications
{
    public class AdditionalSetup : IWantCustomInitialization
    {
        public void Init()
        {
            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c =>
            {
                c.For<INotificationComponent>()
                    .Use<DbNotifier>();
            });

        }
    }
}
