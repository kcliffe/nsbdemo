﻿using System;

namespace NsbDemo.Notifications.Notifiers
{
    public interface INotificationComponent
    {
        void Deliver(string content, string to, string subject);
    }
}
