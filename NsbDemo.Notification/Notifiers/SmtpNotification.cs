﻿using System.Net.Mail;

namespace NsbDemo.Notifications
{
    public class SmtpNotifications : NsbDemo.Notifications.INotificationComponent
    {
        /// <summary>
        /// Simple mail delivery implementation :-)
        /// </summary>
        public void Deliver(string content, string to, string subject)
        {
            using (var client = new SmtpClient())
            {
                var msg = new MailMessage(
                    "processing@nsbdemocard.com",
                    to,
                    subject,
                    content);

                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("nsbdemo1@gmail.com", "nsbDemo_1");   
                client.Port = 587;
                client.Send(msg);
            }
        }
    }
}
