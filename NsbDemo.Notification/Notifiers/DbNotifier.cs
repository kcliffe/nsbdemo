﻿using NsbDemo.Core.Entities;
using Raven.Client;
using System;

namespace NsbDemo.Notifications.Notifiers
{
    internal class DbNotifier : INotificationComponent
    {
        IDocumentSession session;

        public DbNotifier(IDocumentSession session)
        {
            this.session = session;
        }

        public void Deliver(string content, string to, string subject)
        {
            session.Store(new SystemNotification {
                To = to,
                Content = content,
                Subject = subject
            });
        }
    }
}
