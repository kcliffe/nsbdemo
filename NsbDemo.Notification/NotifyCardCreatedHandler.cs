﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NServiceBus;
using NsbDemo.Contracts.Events;
using Raven.Client;
using NsbDemo.Contracts.Integration.Events;

namespace NsbDemo.Notifications
{
    public class NotifyCardCreatedHandler : IHandleMessages<NsbDemoCardCreatedEvent>
    {
        private readonly IDocumentSession session;
        private readonly INotificationComponent notifier;

        public NotifyCardCreatedHandler(IDocumentSession session, INotificationComponent notifier)
        {
            this.session = session;
            this.notifier = notifier;
        }

        public void Handle(NsbDemoCardCreatedEvent @event)
        {
            var member = session.Load<Member>(@event.MemberId);

            // Create a templated notification message
            const string notification = "<p>Hi @Model.Title @Model.Name,</p><p>Congratulations - you'll shortly be receiving your membership card!.</p>";
            // new TemplateContent(session).Fetch("systemNotifications/ApplicationReceived");
           
            // Fully resolve context for templated email
            var context = new {
                Title = member.Title,
                Name = member.Name,                
            };

            var template = new RazorEngine.Templating.TemplateService();
            var resolvedContent = template.Parse(notification, context, null, null);

            // Hardcoded delivery for demo
            notifier.Deliver(resolvedContent, member.EmailAddress, "You card is on the way!");

            Helpers.Out("Notified applicant {0} card has been printed", member.EmailAddress);
        }
    }
}
