﻿using System;

namespace NsbDemo.Notifications
{
    public interface INotificationComponent
    {
        void Deliver(string content, string to, string subject);
    }
}
