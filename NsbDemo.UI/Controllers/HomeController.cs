﻿using NServiceBus;
using Raven.Client;
using System.Web.Mvc;

namespace NsbDemo.UI.Controllers
{
    public class HomeController : Controller
    {
        readonly IDocumentSession session;

        public HomeController(IDocumentSession session)
        {
            this.session = session;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome the the new improved NsbDemo portal!";

            ViewBag.MemberQualifiesForGoldCard = false;
            if (User.Identity.IsAuthenticated)
            {
                var member = new UIMember(session).Get(User.Identity.Name);

                if (member != null)
                {
                    ViewBag.MemberQualifiesForGoldCard = member.QualifiesAsGoldCardMember;
                    ViewBag.MembershipNumber = member.MembershipNumber;
                }
            }

            return View();
        }
    }

    public static class Helpers
    {
        public static IBus GetBus()
        {
            return Configure
             // Tell NSB to search ALL assemblies in the working folder for message handlers
             .With()
             // Use the default IOC container (AutoFAC)
             .DefaultBuilder()
             // Select the MSMQ transport
             .MsmqTransport()
                // Use transaction bus (normally false for "clients")
                .IsTransactional(false)
             .UnicastBus()
                .ImpersonateSender(false)
             // Use the Json message serializer (default is XML)
             .JsonSerializer()
             // As this is a web client, we won't be receiving messages, we're explicitly a producer (smart clients may host consumers)
             .SendOnly();
        }
    }
}
