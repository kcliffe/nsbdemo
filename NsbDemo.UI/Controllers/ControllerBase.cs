﻿using Raven.Client;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace NsbDemo.UI.Controllers
{
    public class ApiControllerBase : ApiController
    {
        protected IDocumentSession DocumentSession;

        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            if (DocumentSession == null)
                DocumentSession = MvcApplication.DocumentStore.OpenSession("BikePartsNZ");

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}
