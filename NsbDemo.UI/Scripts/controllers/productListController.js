﻿ProductListCtrl.$inject = ['$scope', '$resource'];

function ProductListCtrl($scope, $resource) {
    var mostPopularProductsQ = $resource('/api/products/', {}, { query: { method: 'GET', params: {}, isArray: true } });
    $scope.mostPopularProducts = [];
    
    mostPopularProductsQ.query(function (pListIn) {
        $scope.mostPopularProducts = pListIn;
    });    
}