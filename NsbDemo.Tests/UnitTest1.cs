﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NsbDemo.Notifications;

namespace NsbDemo.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            INotificationComponent smtp = new SmtpNotifications();
            smtp.Deliver("test", "kelly.cliffe@gmail.com", "test");
        }
    }
}
