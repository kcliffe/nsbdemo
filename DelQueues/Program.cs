﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;

namespace DelQueues
{
    class Program
    {
        private const string PrivateQueueNamePre = "private$\\";

        static void Main(string[] args)
        {
            //switch (args[0])
            //{                
            //    case "d": DeletePrivateQueues(args[1]);
            //        break;
            //}

            DeletePrivateQueues("NsbDemo*");
            Console.ReadLine();
        }

        private static void DeletePrivateQueues(string queueMatchSpec)
        {
            var privateQueues =
             MessageQueue.GetPrivateQueuesByMachine(Environment.MachineName);

            var toDeletePaths = new List<string>();
            queueMatchSpec = (PrivateQueueNamePre + queueMatchSpec);
            var hasWildCard = queueMatchSpec.EndsWith("*");
            if (hasWildCard)
                queueMatchSpec = queueMatchSpec.Substring(0, queueMatchSpec.Length - 1);

            foreach (var q in privateQueues)
            {
                var match = hasWildCard ? q.QueueName.StartsWith(queueMatchSpec, StringComparison.InvariantCultureIgnoreCase) : q.QueueName.Equals(queueMatchSpec);
                if (match) toDeletePaths.Add(q.Path);
            }

            Console.WriteLine("About to delete {0} queues", toDeletePaths.Count());
            Console.ReadKey();

            if (toDeletePaths.Any())
            {
                toDeletePaths.ForEach( q => { 
                    MessageQueue.Delete(q); Console.WriteLine("Deleting {0}", q); 
                });
            }
        }
    }
}
