﻿using NsbDemo.Core.Entities;

namespace NsbDemo.Integration
{
    public interface ICardService
    {
        Member CreateMember(Application application);
        NsbDemoMemberCard CreateCard(string memberId);
    }
}
