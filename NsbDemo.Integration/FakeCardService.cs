﻿using NsbDemo.Core.Entities;
using Raven.Client;

namespace NsbDemo.Integration
{
    public class FakeCardService : ICardService
    {
        private readonly IDocumentSession session;

        public FakeCardService(IDocumentSession session)
        {
            this.session = session;
        }

        public Member CreateMember(Application application)
        {
            var member = new Member(application);
            session.Store(member);

            return member;
        }

        public NsbDemoMemberCard CreateCard(string memberId)
        {            
            var card = new NsbDemoMemberCard(memberId);
            session.Store(card);

            return card;
        }
    }
}
