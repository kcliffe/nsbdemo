﻿using NsbDemo.Contracts.Commands;
using NsdDemo.Transport.Messages;
using NServiceBus;

namespace NsbDemo.Integration
{
    public class ValidateGoldCardApplicationHandler : IHandleMessages<ValidateGoldCardApplicationCommand>
    {
        public IBus Bus { get; set; }

        public void Handle(ValidateGoldCardApplicationCommand message)
        {
            // TODO. If memberId == 999 throw for 2nd level exception handling
            using (var creditCheckervice = new CreditService.CreditServiceClient())
            {
                var response = creditCheckervice.CheckMemberCredit(new CreditService.CreditCheckRequest 
                {
                    MemberId = message.MembershipNumber
                });

                Bus.Reply(new GoldCardCreditCheckedResponseMessage { 
                    MemberId = message.MembershipNumber,
                    CreditOk = response.CreditOk
                });
            }
        }
    }
}
