﻿namespace NsbDemo.Integration
{
    public class NsbDemoMemberCard
    {
        public string Id { get; set; }
        public string MemberId { get; set; }

        public NsbDemoMemberCard(string memberId)
        {
            MemberId = memberId;
        }
    }
}
