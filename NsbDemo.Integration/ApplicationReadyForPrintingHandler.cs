﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NServiceBus;
using NsbDemo.Contracts.Events;
using Raven.Client;
using NsbDemo.Contracts.Events;
using NsbDemo.Contracts.Integration.Events;

namespace NsbDemo.Integration
{
    public class ApplicationReadyForPrintingHandler : IHandleMessages<ApplicationReadyForPrintingCommand>
    {        
        private readonly ICardService fakeCardService;
        private readonly IDocumentSession session;
        public IBus Bus { get; set; }

        public ApplicationReadyForPrintingHandler(IDocumentSession session, ICardService cardService)
        {
            this.session = session;
            fakeCardService = cardService;
        }

        public void Handle(ApplicationReadyForPrintingCommand command)
        {
            var application = session.Load<Application>(command.ApplicationReference);

            // Some printy stuff. Maybe a service call here - see the goldcard saga
            // This call will transition the Applicant to a Member
            var member = fakeCardService.CreateMember(application);
            var card = fakeCardService.CreateCard(member.Id);

            Bus.Publish(new NsbDemoCardCreatedEvent(member.Id, card.Id));

            Helpers.Out("Card {0} created for new member {0}", card.Id, application.Applicant.Name);
        }
    }
}
