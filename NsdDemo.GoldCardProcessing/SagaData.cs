﻿using NServiceBus.Saga;
using System;

namespace NsdDemo.GoldCardProcessing
{
    public class SagaData : IContainSagaData
    {
        public Guid Id { get; set; }

        public string OriginalMessageId { get; set; }

        public string Originator { get; set; }
      
        /// <summary>
        /// Help NServiceBus by letting it know that only one saga exists for each MemberId
        /// </summary>
        [Unique]
        public string MembershipNumber { get; set; }

        public bool Message2Arrived
        {
            get { return false; }
            set { }
        }
    }
}
