﻿using NsbDemo.Contracts.Commands;
using NsbDemo.Contracts.Events;
using NsbDemo.Contracts.Messages;
using NsdDemo.Transport.Messages;
using NServiceBus;
using NServiceBus.Saga;
using Raven.Client;
using System;
using System.Linq;

namespace NsdDemo.GoldCardProcessing
{
    /// <summary>
    /// Implement a (_possibly_) long running set of operations. In this case validate a gold card member.    
    /// Note - the saga is a pure conductor. No implementation, no state changes. Helps debugging.
    /// </summary>
    public class GoldCardValidationSaga : Saga<SagaData>,
        IAmStartedByMessages<ReceiveGoldCardApplicationCommand>,
        IHandleMessages<GoldCardCreditCheckedResponseMessage>,
        IHandleMessages<ApproveGoldCardResponseMessage>
    {
        readonly IDocumentSession session;

        public GoldCardValidationSaga()
        {
        }

        public GoldCardValidationSaga(IDocumentSession session)
        {
            this.session = session;
        }

        public override void ConfigureHowToFindSaga()
        {
            // When we receive a GoldCardCreditChecked message, activate saga by correlating memberId
            ConfigureMapping<GoldCardCreditCheckedResponseMessage>(sagaData => 
                sagaData.MembershipNumber, goldCardCreditCheckedResponseMessage => goldCardCreditCheckedResponseMessage.MemberId);

            ConfigureMapping<ApproveGoldCardResponseMessage>(sagaData => 
                sagaData.MembershipNumber, approveGoldCardResponseMessage => approveGoldCardResponseMessage.MemberId);
        }

        public void Handle(ReceiveGoldCardApplicationCommand command)
        {
            // Have to ensure here that a duplicate saga cannot be instantiated.
            if (GoldCardApplicationRecieved(command.MembershipNumber))
                return;

            // Insert any properties that we might need to correlate on (or might be useful during saga processing) into saga data
            Data.MembershipNumber = command.MembershipNumber;

            // Reuse the GoldCardValidationRequestCommand?
            Bus.Send(new ValidateGoldCardApplicationCommand(command.MembershipNumber));

            // At some stage (possibly after multiple 2nd level retries) we'll receive a response     
            // If not within a specified interval, timeout 
            RequestUtcTimeout<MyCustomTimeoutState>(TimeSpan.FromHours(1));
        }

        public void Handle(GoldCardCreditCheckedResponseMessage response)
        {
            // b) Voila!
            // Note that the correct saga instance has been "rehydrated" automatically.
            if (response.CreditOk)
            {
                // Tell the orinator we're good with the credit check
                ReplyToOriginator(new GoldCardCreditCheckOkMessage(response.MemberId));                

                // Next phase, we need the physical Ok from somebody in Management... these Gold cards are a big deal...
                Bus.Send(new ApproveGoldCardApplicationCommand(response.MemberId));
            }
            else
            {
                Bus.Publish(new GoldCardCreditCheckFailedEvent(response.MemberId));
            }            
        }

        public void Handle(ApproveGoldCardResponseMessage responseMessage)
        {
            // We're being pretty SRP with these messages - we might just publish a single GoldCardApprovalStatusRecievedEvent with a status
            // to indicate approval / rejection.
            if (responseMessage.Approved)
            {
                Bus.Publish(new GoldCardApprovedEvent(responseMessage.MemberId));
            }
            else
            {
                Bus.Publish(new GoldCardRejectedEvent(responseMessage.MemberId));
            }

            // Saga done.
            MarkAsComplete();
        }

        public void Timeout(MyCustomTimeoutState state)
        {
            // TODO. Status for Message2Arrived
            if (!Data.Message2Arrived)
                ReplyToOriginator(new GoldCardValidationRequestExpiredEvent(state.MemberId));
        }

        private bool GoldCardApplicationRecieved(string membershipNumber)
        {
            return session.Query<SagaData>().FirstOrDefault(s => s.MembershipNumber == membershipNumber) != null;
        }
    }

    public class MyCustomTimeoutState
    {
        public string MemberId { get; set; }
    }
}
