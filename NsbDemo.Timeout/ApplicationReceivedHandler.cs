﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NsbDemo.Contracts.Events;
using NServiceBus;
using Raven.Client;
using System;

namespace NsbDemo.Timeout
{
    // This endpoint is a subscriber to the ApplicationReceived event published by the processing endpoint
    public class ApplicationReceivedHandler : IHandleMessages<ApplicationReceivedEvent>
    {        
        private readonly IDocumentSession session;
        public IBus Bus { get; set; }

        public ApplicationReceivedHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(ApplicationReceivedEvent @event)
        {
            var application = session.Load<Application>(@event.ApplicationReference);
            application.Recieve();

            // The seconds value would be read from config...
            var processAt = DateTime.Now.Add(new TimeSpan(0, 0, 0, 10));

            // Send the TimeoutApplication message after our user defined period has expired.
            var timeoutApplicationMessage = new TimeoutApplicationCommand(@event.ApplicationReference);
            Bus.Defer(processAt, timeoutApplicationMessage);

            Helpers.Out("Recieved application {0} - waiting until {1} for image", @event.ApplicationReference, processAt);
        }
    }
}
