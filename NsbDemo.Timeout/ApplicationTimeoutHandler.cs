﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NServiceBus;
using Raven.Client;

namespace NsbDemo.Timeout
{
    // Consume a message which indicates that an application has timed out because a photo has not arrived.
    public class ApplicationTimeoutHandler : IHandleMessages<TimeoutApplicationCommand>
    {        
        private readonly IDocumentSession session;
        public IBus Bus { get; set; }

        public ApplicationTimeoutHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(TimeoutApplicationCommand command)
        {
            var application = session.Load<Application>(command.ApplicationReference);
            if (application.IsStillPendingImage)
            {
                // We've still not received a valid photo Id, expire the application, this should then turn up on some sort of "alerts dashboard"
                Helpers.Out("Expiring application {0}", command.ApplicationReference);
                application.Expire();
                return;
            }

            // Otherwise - progress to "rules engine" for processing of the application
            Bus.Send(new ProcessApplicationCommand(command.ApplicationReference));

            Helpers.Out("Image received for application {0}", command.ApplicationReference);
        }
    }
}
