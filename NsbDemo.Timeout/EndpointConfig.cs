using NsbDemo.Core;
using NServiceBus;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;
using StructureMap;
using System;
using System.Configuration;
using System.Reflection;

namespace NsbDemo.Timeout
{
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Server, IWantCustomInitialization, IWantToRunAtStartup
    {
        /// <summary>
        /// Configure NServiceBus
        /// </summary>
        public void Init()
        {
            Configure
                // Only check this assembly for handlers
                .With(Assembly.GetExecutingAssembly())
                // IOC with Structuremap
                .StructureMapBuilder()
                .MsmqTransport()
                .RavenPersistence("NsbDemoDatabaseUrl", "NsbDemo")
                //.InMemorySubscriptionStorage()
                .UseInMemoryTimeoutPersister()
                .JsonSerializer()
                // Message defined in this namespace, unobtrusive (don't need to implement IMessage, ICommand..)
                .DefiningMessagesAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Messages"))
                .DefiningCommandsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Commands"))
                .DefiningEventsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Events"))
                .UnicastBus();
        }


        public void Run()
        {
            Helpers.Out("Timeout service ready");
        }


        public void Stop()
        {
            // throw new NotImplementedException();
        }
    }

    public class SetupContainer : IWantCustomInitialization
    {
        public IBus Bus { get; set; }

        public void Init()
        {
            var store = new DocumentStore { ConnectionStringName = "NsbDemoDatabaseUrl" };
            store.ResourceManagerId = Guid.NewGuid();
            store.Initialize();

            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c =>
            {
                c.For<IDocumentStore>()
                    .Singleton()
                    .Use(store);

                c.For<IDocumentSession>()
                     .Use(ctx =>
                     {
                         var session = ctx.GetInstance<IDocumentStore>().OpenSession("NsbDemo");
                         session.Advanced.AllowNonAuthoritativeInformation = false;
                         return session;
                     });

                c.For<IManageUnitsOfWork>()
                    .Use<RavenUnitOfWork>();               
            });

        }
    }
}
