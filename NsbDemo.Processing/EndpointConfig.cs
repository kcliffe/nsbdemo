using NsbDemo.Core;
using NServiceBus;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;
using StructureMap;
using System;
using System.Reflection;

namespace NsbDemo.Processing
{
    // There is ONE class that implements IConfigure this endpoint per assembly
    //
    // IConfigureThisEndpoint is a marker interface used by NSB
    // ASA_Publisher is a role that activates one or more role specific features
    // IWantCustomInitialization allows us to configure NSB via INIT() method
    // IWantToRunAtStartup gives us opportunity to perform actions when bus is started and stopped.
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Publisher, IWantCustomInitialization, IWantToRunAtStartup
    {
        public void Init()
        {
            Configure
                // Only check this assembly for handlers
                .With(Assembly.GetExecutingAssembly())
                // IOC with Structuremap
                .StructureMapBuilder()
                .MsmqTransport()
                // Tell NSB to use Raven when it needs to persist metadata
                .RavenPersistence("NsbDemoDatabaseUrl", "NsbDemo")
                // In particular - subscriptions (see pub / sub)
                .RavenSubscriptionStorage()
                //.InMemorySubscriptionStorage()
                // Use transient store for message delivery timeouts
                .UseInMemoryTimeoutPersister()
                .JsonSerializer()
                // Configure unobtrusive modes (don't need to implement IMessage, ICommand..)
                // This means that contract assemblies do not need NSB references
                .DefiningMessagesAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Messages"))
                .DefiningCommandsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Commands"))
                .DefiningEventsAs(t => t.Namespace != null && t.Namespace.StartsWith("NsbDemo") && t.Namespace.EndsWith("Events"))
                .UnicastBus();
        }

        public void Run()
        {
            Helpers.Out("Processing service ready");
        }

        public void Stop()
        {
        }
    }

    /// <summary>
    /// Init the IOC Container
    /// </summary>
    public class SetupContainer : IWantCustomInitialization
    {
        public IBus Bus { get; set; }

        public void Init()
        {
            var store = new DocumentStore {
                ConnectionStringName = "NsbDemoDatabaseUrl", 
                ResourceManagerId = Guid.NewGuid()
            };
            store.Initialize();

            // Wire up StructureMap to perform ctor / property injection for handlers
            ObjectFactory.Configure(c =>
            {
                c.For<IDocumentStore>()
                    .Singleton()
                    .Use(store);

                c.For<IDocumentSession>()
                     .Use(ctx =>
                     {
                         var session = ctx.GetInstance<IDocumentStore>().OpenSession("NsbDemo");
                         session.Advanced.AllowNonAuthoritativeInformation = false;
                         return session;
                     });

                c.For<IManageUnitsOfWork>()
                    .Use<RavenUnitOfWork>();               
            });

        }
    }
}
