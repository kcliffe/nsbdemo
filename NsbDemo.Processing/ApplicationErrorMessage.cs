﻿namespace NsbDemo.Processing
{
    class ApplicationErrorMessage
    {
        public string ErrorMessage { get; set; }

        public ApplicationErrorMessage(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}
