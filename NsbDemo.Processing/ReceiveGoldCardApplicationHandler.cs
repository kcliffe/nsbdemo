﻿using System.Linq;
using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NsbDemo.Contracts.Events;
using NServiceBus;
using Raven.Client;

namespace NsbDemo.Processing
{
    public class ReceiveGoldCardApplicationHandler : IHandleMessages<ReceiveGoldCardApplicationCommand>
    {
        readonly IDocumentSession Session;
        public IBus Bus { get; set; }

        public ReceiveGoldCardApplicationHandler(IDocumentSession session)
        {
            Session = session;
        }

        public void Handle(ReceiveGoldCardApplicationCommand command)
        {
            Helpers.Out("Received gold card request for {0}", command.MembershipNumber);

            var member = Session.Query<Member>().First(m => m.EmailAddress == command.MembershipNumber);
            if (member.QualifiesAsGoldCardMember)
            {
                Bus.Send(new CreateGoldCardApplicationCommand(command.MembershipNumber));
            }
            else
            {
                Bus.Publish(new GoldCardApplicationFailedEvent(command.MembershipNumber));
            }
        }
    }
}
