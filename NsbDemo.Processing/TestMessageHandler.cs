﻿using NsbDemo.Contracts.Messages;
using NServiceBus;
using Raven.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NsbDemo.Processing
{
    class TestMessageHandler : IHandleMessages<TestMessage>
    {
        public IBus Bus { get; set; }

        private readonly IDocumentSession session;

        public TestMessageHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(TestMessage message)
        {
            Bus.Send(new TestMessage());

            session.Store(new { Title = "TestEntity" });

            throw new Exception("Should rollback");
        }
    }
}
