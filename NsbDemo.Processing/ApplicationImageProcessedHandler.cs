﻿using NServiceBus;
using NsbDemo.Core;
using NsbDemo.Contracts.Commands;
using Raven.Client;
using NsbDemo.Core.Entities;

namespace NsbDemo.Processing
{   
    /// <summary>
    /// We're talking past tense, this should be an event
    /// </summary>
    public class ApplicationImageProcessedHandler : IHandleMessages<ApplicationImageProcessedCommand>
    {
        private readonly IDocumentSession session;
        public IBus Bus { get; set; }

        public ApplicationImageProcessedHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(ApplicationImageProcessedCommand command)
        {
            var application = session.Load<Application>(command.ApplicationReference);
            application.ImageProcessed();

            if (command.SendResponse)
                Bus.Return(1);

            Helpers.Out("Image processed for application {0}", command.ApplicationReference);
        }
    }
}
