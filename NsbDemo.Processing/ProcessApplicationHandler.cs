﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NsbDemo.Contracts.Events;
using NServiceBus;
using Raven.Client;
using System;

namespace NsbDemo.Processing
{   
    public class ProcessApplicationHandler : IHandleMessages<ProcessApplicationCommand>
    {        
        private readonly IDocumentSession session;
        public IBus Bus { get; set; }

        public ProcessApplicationHandler(IDocumentSession session)
        {
            this.session = session;
        }

        public void Handle(ProcessApplicationCommand command)
        {           
            var application = session.Load<Application>(command.ApplicationReference);

            Helpers.Out("Application {0} processing ...", command.ApplicationReference);
            
            // Will update the application status - the unit of work will persist the application and commit the Tx
            if (application.Process())
            {
                // ... non transactional calls to card service for printing etc
                // moved to transactional 
                // cardService.CreateMemberAndCard();

                // Update the 
                Bus.Send(new ApplicationReadyForPrintingCommand(command.ApplicationReference));
            }

            //throw new Exception("Oops..deadlock or something!");

            Helpers.Out("Application {0} processed", command.ApplicationReference);
        }
    }
}
