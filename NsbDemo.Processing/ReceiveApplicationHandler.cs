﻿using NsbDemo.Core;
using NsbDemo.Core.Entities;
using NsbDemo.Contracts.Commands;
using NsbDemo.Contracts.Events;
using NServiceBus;
using Raven.Client;
using System;
using System.Linq;

namespace NsbDemo.Processing
{
    // A handler is a .net class which implements the interface IHandleMessages
    // A class *could* implement multiple handlers, but by convention we do not. 
    // That's where SAGA's come in.
    public class ReceiveApplicationHandler : IHandleMessages<ReceiveApplicationCommand>
    {        
        private readonly IDocumentSession session;

        // Our go-to guy, the bus is injected via the IOC container (see the EndpointConfiguration class)
        public IBus Bus { get; set; }

        // Same for our ravendb session
        public ReceiveApplicationHandler(IDocumentSession session)
        {
            this.session = session;
        }

        // This is the guts of the handler - the implementation of the IHandleMessage interface.
        public void Handle(ReceiveApplicationCommand command)
        {
            // Prevent applications from a user with the same email address.
            var existingApp = session
                .Query<Application>()
                .FirstOrDefault(app => app.Applicant.EmailAddress.Equals(command.EmailAddress, StringComparison.InvariantCulture));

            if (existingApp != null)
            {
                // Somebody will be responsible for messages arriving at this queue :-)
                Bus.Publish(new ApplicationErrorMessage("Duplicate application"));
                return;
            }

            // Ok. Create the application
            var application = Application.Create(command.Title, command.FirstName, command.LastName, command.RefereeCardReference,
                command.AddressLine1, command.AddressLine2, command.PostCode, command.City, command.EmailAddress);
            
            session.Store(application);

            // Publish the fact an aplication was created. Note. It's an EVENT. Some stage change thats already happened.
            var applicationReceivedEvent = new ApplicationReceivedEvent(application.Id);
            Bus.Publish(applicationReceivedEvent);

            if (command.PostCode == "0000")
            {
                throw new Exception("Demonstrating 1st level retries / fault tolerance");
            }

            Helpers.Out("Received application from {0}", application.Applicant.EmailAddress);
        }
    }
}
