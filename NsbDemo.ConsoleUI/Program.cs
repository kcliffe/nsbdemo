﻿using NsbDemo.Transport.Commands;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NsbDemo.ConsoleUI
{
    class Program
    {
        public static IBus Bus { get; set;}

        static void Main(string[] args)
        {
            // Init the bus
            Bus = Helpers.CreateBus();

            bool exit = false;

            while (!exit)
            {
                ShowMenu();

                var key = Console.ReadKey();
                switch (key.KeyChar)
                {
                    case 'c': CreateApplication(); break;
                    case 's': ProcessImage(); break;
                    case 'q': exit = true; break;
                }
            }
        }

        private static void CreateApplication()
        {
            Console.WriteLine();
            Console.WriteLine("Create Application");
            Console.WriteLine("Email address: ");
            var emailAddress = Console.ReadLine();

            Bus.Send(new ReceiveApplicationCommand("Mr", "Kelly", "Cliffe", "Card00001", "Here", "There", "8080", "Chch", emailAddress));
            Console.WriteLine("ReceiveApplication command sent");
        }

        private static void ProcessImage()
        {
            Console.WriteLine();
            Console.WriteLine("Process Image");
            Console.WriteLine("Application: ");
            var applicationIdentifier = Console.ReadLine();

            Bus.Send(new ApplicationImageProcessedCommand(applicationIdentifier));
            Console.WriteLine("Image processing command sent");
        }

        private static void ShowMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Enter a command:");
            Console.WriteLine("(C)reate application");
            Console.WriteLine("(S)imulate image processing:");
            //Console.WriteLine("(S)imulate image processing:");
        }
    }

    public static class Helpers
    {
        public static IBus CreateBus()
        {
            return Configure
                .With()
                .DefaultBuilder()
                .MsmqTransport()
                .IsTransactional(true)
                .UnicastBus()
                .JsonSerializer()
                .SendOnly();
        }
    }
}
